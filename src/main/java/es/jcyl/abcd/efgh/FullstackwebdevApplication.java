package es.jcyl.abcd.efgh;

import java.util.*;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("unused")
@SpringBootApplication
public class FullstackwebdevApplication {

	public static void main(String[] args) {
		SpringApplication.run(FullstackwebdevApplication.class, args);		
		
	}
}

