package es.jcyl.abcd.efgh.persistencia.conversores;


import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import es.jcyl.abcd.efgh.persistencia.entidades.TipoVia;

@Converter
public class TipoViaConversor  implements AttributeConverter <TipoVia, String>{
	
	@Override
	public String convertToDatabaseColumn(TipoVia attribute) {
		
		if ( attribute == null) return null;
		
		switch ( attribute ) {
		case CALLE: return "CL";
		case PASEO: return "PS";
		case AVENIDA: return "AV";
		case CAMINO: return "CO";
		case RONDA:  return "RO";
		
		default: throw new IllegalArgumentException ();
		 
		}
	}

	@Override
	public TipoVia convertToEntityAttribute(String dbData) {
		
		if (dbData == null) return null;
		
		switch ( dbData ) {
		case "CL": return TipoVia.CALLE;
		case "PS": return TipoVia.PASEO;
		case "AV": return TipoVia.AVENIDA;
		case "CO": return TipoVia.CAMINO;
		case "RO": return TipoVia.RONDA;
		
		
		default: throw new IllegalArgumentException ();
		
		}
	}

}
