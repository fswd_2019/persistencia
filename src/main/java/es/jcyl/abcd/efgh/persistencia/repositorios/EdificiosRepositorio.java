package es.jcyl.abcd.efgh.persistencia.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.jcyl.abcd.efgh.persistencia.entidades.EdificioEntidad;
import es.jcyl.abcd.efgh.persistencia.entidades.TipoVia;

@Repository
public interface EdificiosRepositorio extends JpaRepository <EdificioEntidad, Integer>, EdificiosRepositorioPersonalizado {
	
	
	// buscar por provincia
	
	default List<EdificioEntidad> buscarPorProvincia ( String prov ) {
		return  buscarPorNombreProvincia (prov);
	}	
	
	@Query ("select edif "
			+ "from EdificioEntidad edif "
			+ "left join edif.direccion.poblacion pob left join pob.provincia prov "
			+ "where upper(prov.provincia) like concat ('%',concat(upper(:prov),'%'))")
	List<EdificioEntidad> buscarPorNombreProvincia ( @Param ("prov") String prov );
	
	
	// buscar por tipo y nombre de vía	
	
	default List<EdificioEntidad> buscarPorVia (TipoVia tipoVia , String direccion) {		
		return findByDireccionTipoViaAndDireccionNombreViaContainsIgnoreCase (  tipoVia ,  direccion);
	}

	List<EdificioEntidad> findByDireccionTipoViaAndDireccionNombreViaContainsIgnoreCase( @Param ("tipoVia") TipoVia tipoVia, @Param ("direccion")String direccion);
	
	
	// buscar los edificios por nombre del edificio
	
	default List<EdificioEntidad> buscarPorNombre (String nombre) {
		return findByNombreContainsIgnoreCase (nombre);
	}
	
	List<EdificioEntidad> findByNombreContainsIgnoreCase (@Param ("nombre") String nombre); 
	
}
