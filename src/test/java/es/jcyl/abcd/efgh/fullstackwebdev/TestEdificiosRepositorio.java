package es.jcyl.abcd.efgh.fullstackwebdev;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.jcyl.abcd.efgh.FullstackwebdevApplication;
import es.jcyl.abcd.efgh.persistencia.entidades.*;
import es.jcyl.abcd.efgh.persistencia.repositorios.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration ( classes = FullstackwebdevApplication.class)
public class TestEdificiosRepositorio {
	
	@Autowired
	private EdificiosRepositorio repo;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Test
	public void testTodosEdificios () throws Exception {
		List<EdificioEntidad> edificios = (List<EdificioEntidad>) repo.findAll();
		
		for ( EdificioEntidad edif: edificios) {
			System.out.println  ( edif.toString() );
		}	
	}
	
	
	@Test
	public void testBusquedaPorProvincia () throws Exception {
		List<EdificioEntidad> edificios = repo.buscarPorProvincia("vall");
		
		assertNotNull ( edificios);
		
		for (EdificioEntidad edificio : edificios ) {
	     	System.out.println( edificio );
		}
		
		assertTrue ( edificios.size() > 0 );
	}
	
	@Test
	public void testBusquedaPorVia () throws Exception {
		List<EdificioEntidad> edificios = repo.buscarPorVia( TipoVia.CALLE, "santiago" );
 
		assertNotNull ( edificios);
		
		for (EdificioEntidad edificio : edificios ) {
			System.out.println( edificio );
		}
		
		assertTrue ( edificios.size() > 0 );
		
	}
	
	@Test
	public void testBusquedaEdificiosSinSalas() {
		
		System.out.println("Buscando edificios sin salas ...");
		List<EdificioEntidad> edificios = repo.buscarEdificiosSinSalas();
		
		assertNotNull ( edificios);
		
		for (EdificioEntidad edificio : edificios ) {
			System.out.println( edificio );
		}
		
		assertTrue ( edificios.size() > 0 );
		
	}
	
	
	@Test
	public void testBuscarPorSalasConCapacidad() {
		

		List<EdificioEntidad> edificios = repo.buscarEdificiosConSalasYConCapacidad(100);
		
		assertNotNull (edificios);
		
		for (EdificioEntidad edificio : edificios ) {
			System.out.println( edificio );
		}

		assertTrue ( edificios.size() > 0);
		
	}
	
	@Test
	public void testBuscarPorNombre() {
		

		List<EdificioEntidad> edificios = repo.buscarPorNombre("hospi");
		
		assertNotNull (edificios);
		
		for (EdificioEntidad edificio : edificios ) {
			System.out.println( edificio );
		}

		assertTrue ( edificios.size() == 1);
		
	}

	
	
	
	
	

}
