package es.jcyl.abcd.efgh.fullstackwebdev;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.jcyl.abcd.efgh.FullstackwebdevApplication;
import es.jcyl.abcd.efgh.persistencia.entidades.*;
import es.jcyl.abcd.efgh.persistencia.repositorios.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration ( classes = FullstackwebdevApplication.class)
public class TestReservasRepositorio {
	@Autowired
	private ReservasRepositorio repo;
	
	
	@Autowired
	private SalasRepositorio repoSalas;
	
	
	@Test
	public void testTodasReservas () throws Exception {
		
		List<ReservaEntidad> reservas = (List<ReservaEntidad>) repo.findAll();
				
		for ( ReservaEntidad reserva: reservas) {
			System.out.println  ( reserva.toString() );
		}

	}
	
	@Test
	public void testDisponibilidadPorSalaFecha () throws Exception {
		
					
		List<ReservaEntidad> reservas = repo.findAll();
		
        assertNotNull (reservas);		
		assertFalse( reservas.isEmpty() );		
		
		
		SalaEntidad sala = reservas.get(0).getSala();		
		Date fecha = reservas.get(0).getFechaReserva();
		
		boolean disponible = !repo.existeReserva( sala , fecha );
		
		assertFalse ( disponible );
		

	}
}
